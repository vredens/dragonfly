<?php

namespace App\API;

use \Slim\Slim;
use \Dragonfly\API\Booter as DragonflyBooter;

/**
 * This is a SAMPLE which you should change into your REST API. The sample is a
 * direct copy of the \Dragonfly\API\Booter.
 */
class Booter extends DragonflyBooter {
	/**
	 * Route registration
	 */
	static protected function router(Slim $app) {
		// Add Profiler middleware
		$app->add(new \Dragonfly\API\Middleware\Profiler());
		// Add Auth middleware which registers a $app->container->user()
		$app->add(new \Dragonfly\API\Middleware\Auth());
		// Add input parser for JSON with a max body size of 500KB
		$app->add(new \Dragonfly\API\Middleware\ContentTypeParser([
			'enable-json' => true,
			'enable-xml' => false,
			'enable-csv' => false,
			'max_size' => 5e5 // 500KB
		]));

		// Define routes
		$app->get('/', function () use ($app) {
			// debug
			$app->log->debug('route [/]');
			// Render index view
			$app->render('index.html');
		});

		$app->group('/api', function () use ($app) {
			// API errors are handled differently
			$app->error(function (\Exception $e) use ($app) {
				return \Dragonfly\API::handleException($e);
			});

			// register routes only if request if a XHR
			//if ($request->isXhr()) {
				//static::registerApiRoutes($app);
			//}
			// ... or just always reply with JSON to requests
			static::registerApiRoutes($app);

			// POKEMON ROUTE for any GET request which is not a XHR
			if (!$app->request->isXhr()) {
				$app->get('/:any', function ($any) use ($app) {
					$app->redirect('/');
				})->conditions(['any' => '.*']);
			}
		});
	}

	/**
	 * Register API routes
	 */
	static private function registerApiRoutes(Slim $app) {
		$app->get('/about', '\Dragonfly\API\RouteHandler\Misc::about');

		$app->group('/app', function () use ($app) {
			$app->get('/settings', '\Dragonfly\API\RouteHandler\Settings::search');
			$app->post('/settings', '\Dragonfly\API\RouteHandler\Settings::set');
			$app->delete('/settings', '\Dragonfly\API\RouteHandler\Settings::delete');
		});

		$app->post('/login', '\Dragonfly\API\RouteHandler\Auth::login');

		$app->post('/logout', '\Dragonfly\API\RouteHandler\Auth::logout');

		$app->group('/user', function () use ($app) {
			$app->get('/profile', '\Dragonfly\API\RouteHandler\User::getProfile');
			$app->get('/avatar', '\Dragonfly\API\RouteHandler\User::getAvatar');
			$app->post('/avatar', '\Dragonfly\API\RouteHandler\User::uploadAvatar');
			$app->get('/:user_id/avatar', '\Dragonfly\API\RouteHandler\User::getAvatar');
			$app->get('/:user_id/profile', '\Dragonfly\API\RouteHandler\User::getPublicProfile');
		});

		$app->group('/file', function () use ($app) {
			$app->get('/:namespace/:id', '\Dragonfly\API\RouteHandler\FileServer::getFile');
			$app->post('/:namespace/:id', '\Dragonfly\API\RouteHandler\FileServer::uploadFile');
		});
	}
}
