<?php

namespace Dragonfly\API;

use \Slim\Slim;

class Booter {
	public static function boot($config) {
		/**
		 * PHP Sessions need to be activated before initializing the Slim App
		 */
		if ($config['app.session.mode'] == 'native') {
			// TODO: convert sessions to permanent storage
			if (!empty($config['app.session.path'])) {
				session_save_path($config['app.session.path']);
			}
			session_name($config['app.session.name']);
			session_start();
		}

		// Set global Logger for the application
		$log = new \Monolog\Logger('dragonfly');
		$log->pushHandler(new \Monolog\Handler\StreamHandler($config['app.log.filename'], \Monolog\Logger::DEBUG));
		\Dragonfly\Logger::set($log);

		// Prepare app
		$app = new \Slim\Slim([
			'mode' => $config['app.mode'],
		]);

		// Only invoked if mode is "production"
		$app->configureMode('production', function () use ($app) {
			$app->config(array(
				'log.enable' => true,
				'debug' => false
			));
		});

		// Only invoked if mode is "development"
		$app->configureMode('development', function () use ($app) {
			$app->config(array(
				'log.enable' => true,
				'debug' => true
			));
		});

		//$app->config('debug', false);

		// store the configuration in the container
		$app->container->config = $config;

		// configure the database factory
		if (!empty($config['databases'])) {
			\Dragonfly\Data\DatabaseFactory::setup($config['databases']);
		}

		// configure the storage factory
		if (!empty($config['storage'])) {
			\Dragonfly\Data\FileStorageFactory::setup($config['storage']);
		}

		// Set Slim's logger, which differs from the Application logger
		$app->container->singleton('log', function () use ($config) {
			$log = new \Monolog\Logger('slim');
			$log->pushHandler(new \Monolog\Handler\StreamHandler($config['slim.log.filename'], \Monolog\Logger::DEBUG));

			return $log;
		});

		// create repository singletons (AKA: cheap factories)
		$app->container->singleton('repo.user', function ($container) {
			return new \Dragonfly\User\Repository(\Dragonfly\Data\DatabaseFactory::getSQLDatabase('user'));
		});
		$app->container->singleton('repo.settings', function ($container) {
			return new \Dragonfly\Settings\Repository(\Dragonfly\Data\DatabaseFactory::getSQLDatabase('default'));
		});

		// Prepare view
		$app->view(new \Slim\Views\Twig());
		$app->view->parserOptions = array(
			'charset' => 'utf-8',
			'cache' => realpath($config['app.templates.cache']),
			'auto_reload' => true,
			'strict_variables' => false,
			'autoescape' => true
		);
		$app->view()->twigTemplateDirs = $config['app.templates.folders'];
		$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());

		// set the application error handler
		$app->error(function (\Exception $e) use ($app) {
			return \Dragonfly\API::handleException($e);
		});

		// register the routes
		self::router($app);

		// Run app
		$app->run();
	}

	/**
	 * Route registration
	 */
	static protected function router(Slim $app) {
		// Add Profiler middleware
		$app->add(new \Dragonfly\API\Middleware\Profiler());
		// Add Auth middleware which registers a $app->container->user()
		$app->add(new \Dragonfly\API\Middleware\Auth());
		// Add input parser for JSON with a max body size of 500KB
		$app->add(new \Dragonfly\API\Middleware\ContentTypeParser([
			'enable-json' => true,
			'enable-xml' => false,
			'enable-csv' => false,
			'max_size' => 5e5 // 500KB
		]));

		// Define routes
		$app->get('/', function () use ($app) {
			// debug
			$app->log->debug('route [/]');
			// Render index view
			$app->render('index.html');
		});

		$app->group('/api', function () use ($app) {
			// API errors are handled differently
			$app->error(function (\Exception $e) use ($app) {
				return \Dragonfly\API::handleException($e);
			});

			// register routes only if request if a XHR
			//if ($request->isXhr()) {
				//static::registerApiRoutes($app);
			//}
			// ... or just always reply with JSON to requests
			static::registerApiRoutes($app);

			// POKEMON ROUTE for any GET request which is not a XHR
			if (!$app->request->isXhr()) {
				$app->get('/:any', function ($any) use ($app) {
					$app->redirect('/');
				})->conditions(['any' => '.*']);
			}
		});

	}

	/**
	 * Register API routes
	 */
	static private function registerApiRoutes(Slim $app) {
		$app->get('/about', '\Dragonfly\API\RouteHandler\Misc::about');

		$app->group('/app', function () use ($app) {
			$app->get('/settings', '\Dragonfly\API\RouteHandler\Settings::search');
			$app->post('/settings', '\Dragonfly\API\RouteHandler\Settings::set');
			$app->delete('/settings', '\Dragonfly\API\RouteHandler\Settings::delete');
		});

		$app->post('/login', '\Dragonfly\API\RouteHandler\Auth::login');

		$app->post('/logout', '\Dragonfly\API\RouteHandler\Auth::logout');

		$app->group('/user', function () use ($app) {
			$app->get('/profile', '\Dragonfly\API\RouteHandler\User::getProfile');
			$app->get('/avatar', '\Dragonfly\API\RouteHandler\User::getAvatar');
			$app->post('/avatar', '\Dragonfly\API\RouteHandler\User::uploadAvatar');
			$app->get('/:user_id/avatar', '\Dragonfly\API\RouteHandler\User::getAvatar');
			$app->get('/:user_id/profile', '\Dragonfly\API\RouteHandler\User::getPublicProfile');
		});

		$app->group('/file', function () use ($app) {
			$app->get('/:namespace/:id', '\Dragonfly\API\RouteHandler\FileServer::getFile');
			$app->post('/:namespace/:id', '\Dragonfly\API\RouteHandler\FileServer::uploadFile');
		});

		$app->group('/dl', function () use ($app) {
			$app->get('/:namespace/:id', '\Dragonfly\API\RouteHandler\FileServer::downloadFile');
		});
	}
}
