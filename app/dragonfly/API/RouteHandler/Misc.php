<?php

namespace Dragonfly\API\RouteHandler;

use \Slim\Slim;

abstract class Misc {
	/**
	 * Returns generic info about the API.
	 *
	 */
	static public function about() {
		$app = Slim::getInstance();

		$user = $app->container->get('user');

		return \Dragonfly\API::reply([
			'copyright' => 'Vredens',
			'anonymous' => $user->isAnonymous(),
		], 200);
	}
}
