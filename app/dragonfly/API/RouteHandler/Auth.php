<?php

namespace Dragonfly\API\RouteHandler;

use \Slim\Slim;
use \Dragonfly\Logger;

abstract class Auth {
	static public function login() {
		$app = Slim::getInstance();
		$container = $app->container;
		$req = $app->request;
		$params = $req->params();

		assert('isset($params["login"])', 'no login parameter');
		assert('isset($params["password"])', 'no password parameter');

		Logger::debug('request [POST][/login]', ['params' => $params, 'test' => 'test']);

		$user = $container->get('user');

		if (! $user->isAnonymous()) {
			throw new \Dragonfly\API\CustomException('Already logged in', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
		}

		$ur = $container->get('repo.user');
		$user = $ur->getByLogin($params['login'], $params['password']);

		if ($user->isAnonymous()) {
			throw new \Dragonfly\API\CustomException('Invalid login', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
		}

		// TODO: implement access token generation and CSRF token
		//$_SESSION['token'] = $user->getToken();
		// TODO: remove below which is just here for kicks
		$_SESSION['token'] = $user->generateToken();
		$_SESSION['user_id'] = $user->get('id');

		return \Dragonfly\API::reply([
			'token' => $_SESSION['token']
		], 200);

	}

	static public function logout() {
		$app = Slim::getInstance();

		Logger::debug('request [POST][/logout]');

		// TODO: remove below which is just here for kicks
		unset($_SESSION['token']);

		return \Dragonfly\API::reply(null, 200);
	}
}
