<?php

namespace Dragonfly\API\RouteHandler;

use \Slim\Slim;
use \Dragonfly\Logger;

abstract class FileServer {
	/**
	 * Handle a request for a user's avatar.
	 *
	 * @param string $namespace The namespace used when storing in the storage system
	 * @param string $id The storage system's id for the file
	 * @param string $storage_name The storage system name (default: 'default')
	 *
	 */
	public static function getFile($namespace, $id, $storage_name = 'default') {
		$app = Slim::getInstance();

		// fetch the storage system (must be configured in the config.php)
		$storage = \Dragonfly\Data\FileStorageFactory::getFileStorage($storage_name);

		// store the uploaded file
		$filedata = $storage->get($namespace, $id);

		if (empty($filedata) or empty($filedata['path'])) {
			throw new \Dragonfly\API\CustomException('File not found.', \Dragonfly\API\HTTPStatusCodes::NOT_FOUND);
		}

		// TODO: check user's access to the file in question

		// send the file back to the client
		$storage->outputViaHTTP($filedata);

		// REVIEW: currently needed since Slim will override response headers
		exit();
	}

	/**
	 * Handle a request for a user's avatar.
	 *
	 * @param string $namespace The namespace used when storing in the storage system
	 * @param string $id The storage system's id for the file
	 * @param string $storage_name The storage system name (default: 'default')
	 *
	 */
	public static function downloadFile($namespace, $id, $storage_name = 'default') {
		$app = Slim::getInstance();

		// fetch the storage system (must be configured in the config.php)
		$storage = \Dragonfly\Data\FileStorageFactory::getFileStorage($storage_name);

		// store the uploaded file
		$filedata = $storage->get($namespace, $id);

		if (empty($filedata) or empty($filedata['path'])) {
			throw new \Dragonfly\API\CustomException('File not found.', \Dragonfly\API\HTTPStatusCodes::NOT_FOUND);
		}

		// TODO: check user's access to the file in question

		// send the file back to the client as attachment
		$storage->outputViaHTTP($filedata, ['attachment' => true]);

		// REVIEW: currently needed since Slim will override response headers
		exit();
	}

	/**
	 * Handle a user's avatar file upload.
	 *
	 * TODO: complete this method
	 *
	 */
	public static function uploadFile($namespace, $id) {
		$app = Slim::getInstance();
		$config = $app->container->get('config');
		$storage_folder = $config['storage']['default']['dir'];

		// fetch the uploaded file data
		$filedata = \Dragonfly\API::processFormUpload('upload', [
			'multi' => false, // we are not expecting multi file uploads, so we restict that
			'supported-mime-types' => [
				'image/jpeg',
				'image/png',
			]
		]);

		Logger::debug('filedata', $filedata);

		switch ($namespace) {
			case 'avatar':
				// fetch the storage system (must be configured in the config.php)
				$storage = \Dragonfly\Data\FileStorageFactory::getFileStorage('avatar');

				// we only allow uploading an avatar for the logged in user
				$user = $app->container->get('user');
				if ($user->isAnonymous()) {
					throw new \Dragonfly\API\CustomException('Unauthorized', \Dragonfly\API\HTTPStatusCodes::UNAUTHORIZED);
				}
				// we use the user id as identifier for the avatar
				$id = $user->get('id');
			break;

			default:
				// fetch the storage system (must be configured in the config.php)
				$storage = \Dragonfly\Data\FileStorageFactory::getFileStorage('default');
			break;
		}

		// store the uploaded file
		$key = $storage->storeUploadedFile($namespace, $id, $filedata);

		return \Dragonfly\API::reply([
			'file' => $key,
			'url' => '/files/' . $key['ns'] . $key['id']
		], 200);
	}
}
