<?php

namespace Dragonfly\API\RouteHandler;

use \Slim\Slim;
use \Dragonfly\Logger;

abstract class Settings {
	/**
	 *
	 */
	static public function search() {
		$app = Slim::getInstance();
		$params = $app->request->params();

		Logger::debug('settings::search.params', ['params' => $params]);

		\Dragonfly\API::assertAdminToken();

		$repo = $app->container->get('repo.settings');

		$settings = $repo->search();

		return \Dragonfly\API::reply([
			'settings' => $settings
		], 200);
	}

	/**
	 *
	 */
	static public function set() {
		$app = Slim::getInstance();
		$params = $app->request->params();

		Logger::debug('settings::set.params', ['params' => $params]);

		\Dragonfly\API::assertAdminToken();

		$repo = $app->container->get('repo.settings');

		// TODO: proper validation of parameters
		if (empty($params['key']) or empty($params['value'])) {
			throw new \Dragonfly\API\CustomException('Bad request.', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
		}

		$repo->set($params['key'], $params['value']);

		return \Dragonfly\API::reply(true, 200);
	}

	/**
	 *
	 */
	static public function delete() {
		$app = Slim::getInstance();
		$params = $app->request->params();

		Logger::debug('settings::set.params', ['params' => $params]);

		\Dragonfly\API::assertAdminToken();

		$repo = $app->container->get('repo.settings');

		// TODO: proper validation of parameters
		if (empty($params['key'])) {
			throw new \Dragonfly\API\CustomException('Bad request.', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
		}

		$repo->delete($params['key']);

		return \Dragonfly\API::reply(true, 200);
	}
}
