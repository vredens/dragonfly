<?php

namespace Dragonfly\API\RouteHandler;

use \Slim\Slim;
use \Dragonfly\Logger;

abstract class User {
	static public function getProfile() {
		$app = Slim::getInstance();
		$container = $app->container;
		$req = $app->request;
		$params = $req->params();

		Logger::debug('getProfile', ['params' => $params]);

		$user = $container->get('user');

		if ($user->isAnonymous()) {
			throw new \Dragonfly\API\CustomException('Unauthorized', \Dragonfly\API\HTTPStatusCodes::UNAUTHORIZED);
		}

		$ur = $container->get('repo.user');
		$ur->fillBasicInfo($user);

		return \Dragonfly\API::reply($user->get(), 200);
	}

	static public function getPublicProfile($user_id) {
		$app = Slim::getInstance();
		$container = $app->container;
		$req = $app->request;
		$params = $req->params();

		Logger::debug('getPublicProfile', ['params' => $params]);

		$ur = $container->get('repo.user');
		$user = $ur->getById($user_id);

		return \Dragonfly\API::reply($user->get(), 200);
	}

	/**
	 * Handle a request for a user's avatar.
	 *
	 */
	static public function getAvatar($user_id = null) {
		$app = Slim::getInstance();
		$config = $app->container['config'];

		if (is_null($user_id)) {
			$user = $app->container->get('user');
			$user_id = $user->get('id');
		}

		// fetch the storage system (must be configured in the config.php)
		$storage = \Dragonfly\Data\FileStorageFactory::getFileStorage('avatar');

		// store the uploaded file
		$filedata = $storage->get('avatar', $user_id);

		Logger::debug('fetched data', ['file' => $filedata ]);

		if (empty($filedata) or empty($filedata['path'])) {
			throw new \Dragonfly\API\CustomException('File not found.', \Dragonfly\API\HTTPStatusCodes::NOT_FOUND);
		}

		// send the file back to the client
		$storage->outputViaHTTP($filedata);

		// REVIEW: currently needed since Slim will override response headers
		exit();
	}

	/**
	 * Handle a user's avatar file upload.
	 *
	 */
	static public function uploadAvatar() {
		$app = Slim::getInstance();
		$config = $app->container->get('config');

		$user = $app->container->get('user');

		if ($user->isAnonymous()) {
			throw new \Dragonfly\API\CustomException('Unauthorized', \Dragonfly\API\HTTPStatusCodes::UNAUTHORIZED);
		}

		// fetch the uploaded file data
		$avatar = \Dragonfly\API::processFormUpload('upload', [
			'multi' => false, // we are not expecting multi file uploads, so we restict that
			'supported-mime-types' => [
				'image/jpeg',
				'image/png',
			]
		]);

		Logger::debug('avatar', $avatar);

		// we use the user id as identifier for the avatar
		$user_id = $user->get('id');

		// fetch the storage system (must be configured in the config.php)
		$storage = \Dragonfly\Data\FileStorageFactory::getFileStorage('avatar');

		// store the uploaded file
		$key = $storage->storeUploadedFile('avatar', $user_id, $avatar);

		$key = '/file/' . $key['ns'] . $key['id'];

		$user->update(['avatar' => $key]);

		// TODO: save the user

		return \Dragonfly\API::reply([
			'user' => $user->get()
		], 200);
	}
}
