<?php

namespace Dragonfly\API\Middleware;

use \Dragonfly\User\User;

class Auth extends \Slim\Middleware {
	public function call() {
		//The Slim application
		$app = $this->app;

		//The Environment object
		$env = $app->environment;

		//The Request object
		$req = $app->request;

		//The Response object
		$res = $app->response;

		// check if there is an authentication token
		if (isset($_SESSION['token'])) {
			// TODO: also use the X-Auth-Token to verify that the client token matches the session one, to avoid CSRF attacks
			$token = $_SESSION['token'];

			$app->container['user'] = new User(['id' => $_SESSION['user_id']]);
		}

		// if no session then check http header 'x-auth-token'
		elseif ($req->headers->has('X-Auth-Token')) {
			$token = $req->headers->get('X-Auth-Token');

			// fetch the user's repository
			$ur = $app->container->get('repo.user');

			// TODO: implement this
			throw new \Dragonfly\API\CustomException('Not Implemented', \Dragonfly\API\HTTPStatusCodes::NOT_IMPLEMENTED);

			// fetch user by token
			$app->container['user'] = $ur->getByToken($token);
		}

		// TODO: handle no token
		else {
			$token = null;

			$app->container['user'] = new User();
		}

		// Run inner middleware and application
		$this->next->call();

		// Capitalize response body
		//$res = $app->response;
		//$body = $res->getBody();
		//$res->setBody(strtoupper($body));
	}
}
