<?php

namespace Dragonfly\API\Middleware;

class ParseException extends \Exception {
}

/**
 * Based on Slim's ContentTypes Middleware
 */
class ContentTypeParser extends \Slim\Middleware {
	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * Constructor
	 * @param array $settings
	 */
	public function __construct($settings = array())
	{
		$defaults = [
			'enable-xml' => false,
			'enable-cvs' => false,
			'enable-json' => true,
			'max_size' => 5e5,
		];
		$this->settings = array_merge($defaults, $settings);
	}

	protected function assertContentMaxLength($content_length) {
		if ($content_length > $this->settings['max_size']) {
			$app->log->error('Content length is too big to be parsed [Content-Length:' . $content_length . ']');
			throw new \Dragonfly\API\CustomException('Payload too large', \Dragonfly\API\HTTPStatusCodes::PAYLOAD_TOO_LARGE);
		}
	}

	public function call() {
		$app = $this->app;
		$req = $app->request();
		$env = $app->environment();

		$mt = $req->getMediaType();
		$cl = $req->getContentLength();

		// ignore empty bodies
		if ($cl == 0) {
			$app->log->debug('Skipping input parser [Content-Length:' . $cl . '][MediaType:' . $mt . ']');
			$this->next->call();
			return;
		}

		$app->log->debug('Parsing input [Content-Length:' . $cl . '][MediaType:' . $mt . ']');

		try {
			/**
			 * Setting 'slim.request.form_hash' will override $app->request()->post()
			 * data source so this becomes transparent to anyone using that method or
			 * the $app->request()->getParams() method to fetch input parameters. Note
			 * that XML and CSV parsers will set 'slim.request.xml' and 'slim.request.csv'
			 * environment variables respectively.
			 */
			switch ($mt) {
				case 'application/json':
					$this->assertContentMaxLength($cl);
					$env['slim.request.form_hash'] = $this->parseJSON($env['slim.input']);
				break;

				case 'application/xml':
				case 'text/xml':
					if ($this->settings['enable-xml']) {
						$this->assertContentMaxLength($cl);
						$env['slim.request.xml'] = $this->parseXML($env['slim.input']);
					}
				break;

				case 'text/csv':
					if ($this->settings['enable-csv']) {
						$this->assertContentMaxLength($cl);
						$env['slim.request.csv'] = $this->parseCSV($env['slim.input']);
					}
				break;

				default:
					$app->log->warn('Failed to find proper content type parser for media type [' . $mt . ']');
				break;
			}

			// Run inner middleware and application
			$this->next->call();

		} catch (ParseException $e) {
			$app->log->error($e);

			return \Dragonfly\API::handleException(new \Dragonfly\API\CustomException('Bad request', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST));
		} catch (\Dragonfly\API\CustomException $e) {
			return \Dragonfly\API::handleException($e);
		}
	}

	/**
	 * Parse JSON
	 *
	 * This method converts the raw JSON input
	 * into an associative array.
	 *
	 * @param  string       $input
	 * @return array|string
	 */
	protected function parseJSON($input)
	{
		if (!function_exists('json_decode')) {
			throw new ParseException('PHP has no JSON decoder available');
		}

		$result = json_decode($input, true);
		if(json_last_error() === JSON_ERROR_NONE) {
			return $result;
		} else {
			throw new ParseException('Content type JSON decode failed: ' . json_last_error_msg(), \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
		}
	}

	/**
	 * Parse XML
	 *
	 * This method creates a SimpleXMLElement
	 * based upon the XML input. If the SimpleXML
	 * extension is not available, the raw input
	 * will be returned unchanged.
	 *
	 * @param  string                  $input
	 * @return \SimpleXMLElement
	 * @throws \Exception
	 */
	protected function parseXML($input)
	{
		if (!class_exists('SimpleXMLElement')) {
			throw new ParseException('PHP has no JSON decoder available');
		}

		try {
			$backup = libxml_disable_entity_loader(true);
			$result = new \SimpleXMLElement($input);
			libxml_disable_entity_loader($backup);
		} catch (\Exception $e) {
			throw new ParseException('Failed to parse XML content type', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST, $e);
		}

		return $result;
	}

	/**
	 * Parse CSV
	 *
	 * This method parses CSV content into a numeric array
	 * containing an array of data for each CSV line.
	 *
	 * @param  string $input
	 * @return array
	 */
	protected function parseCSV($input)
	{
		$temp = fopen('php://memory', 'rw');
		fwrite($temp, $input);
		fseek($temp, 0);
		$res = array();
		while (($data = fgetcsv($temp)) !== false) {
			$res[] = $data;
		}
		fclose($temp);

		return $res;
	}

}
