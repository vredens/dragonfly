<?php

namespace Dragonfly\API\Middleware;

use \Dragonfly\User\User;

class Profiler extends \Slim\Middleware {
	public function call() {
		$req = $this->app->request;
		$res = $this->app->response;

		if (isset($_SERVER['REQUEST_TIME_FLOAT'])) {
			$start = $_SERVER['REQUEST_TIME_FLOAT'];
		} else {
			$start = microtime(true);
		}

		// Run inner middleware and application
		$this->next->call();

		$namespace = $req->getMethod() . ':' . $req->getResourceUri() . ':' . $res->getStatus();
		$time = (microtime(true) - $start) * 1000;

		\Dragonfly\Logger::debug('profiler['.$namespace.']['.$time.'ms]', [ 't' => $time, 'ns' => $namespace ]);
	}
}
