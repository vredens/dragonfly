<?php

namespace Dragonfly\API;

class CustomException extends \Exception {
	protected $data = null;

	/**
	 * Construct an Exception where the code is a valid HTTP Status Code for
	 * Client or Server errors (400 or 500 ranges, respectively).
	 *
	 * @param string     [$message]   Defaults to null.
	 * @param integer    [$code]      The HTTP Status Code. Defaults to 500 (internal server error).
	 * @param array      [$data]      Extra error information data. Defaults to null.
	 * @param \Exception [$previous]  Defaults to null.
	 */
	public function __construct($message = null, $code = 500, $data = null, \Exception $previous = null) {
		assert('$code < 600', '$code must less than 600');
		assert('$code >= 400', '$code must higher or equal to 400');

		if (isset($data)) {
			assert('is_array($data)', '$data must be an array (indexed or not)');
			$this->data = $data;
		}

		parent::__construct($message, $code, $previous);
	}

	public function hasData() {
		return isset($this->data);
	}

	public function getData() {
		return $this->data;
	}
}
