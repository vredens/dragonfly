<?php

namespace Dragonfly\API;

class CustomException extends \Exception {
	/**
	 * Construct an Exception where the code is a valid HTTP Status Code for
	 * Client or Server errors (400 or 500 ranges, respectively).
	 *
	 * @param string     [$message]   Defaults to null.
	 * @param integer    [$code]      The HTTP Status Code. Defaults to 500 (internal server error).
	 * @param \Exception [$previous]  Defaults to null.
	 */
	public function __construct($message = null, $code = 500, \Exception $previous = null) {
		assert('$code < 600', '$code must less than 600');
		assert('$code >= 400', '$code must higher or equal to 400');

		parent::__construct($message, $code, $previous);
	}
}
