<?php

namespace Dragonfly\Generic;

abstract class AppObject {
	protected $data = [];

	/**
	 * Constructor.
	 *
	 * @param array $data
	 */
	public function __construct($data = []) {
		assert('is_array($data)', 'User constructor called with invalid parameters');

		$this->data = $data;
	}

	/**
	 * Get the entire data structure or a single field.
	 *
	 * @param string [$field] If set returns only the value for this field
	 * @return mixed
	 */
	public function get($field = null) {
		if (isset($field)) { return $this->data[$field]; }

		return $this->data;
	}

	/**
	 * Update the data structure merging the current data structure with the
	 * one passed as argument.
	 *
	 * @param array $data  A data structure with the updated fields
	 */
	public function update($data) {
		assert('is_array($data)', 'Update called with invalid parameters');

		$this->data = array_merge($this->data, $data);
	}

	/**
	 * Resets the data structure to the one passed as argument.
	 *
	 * @param array $data  A data structure with the updated fields
	 */
	public function set($data) {
		assert('is_array($data)', 'Set called with invalid parameters');

		$this->data = $data;
	}

	/**
	 * Returns a list of fields by combining included and excluded infoblocks.
	 *
	 * An info block is an array of arrays of strings. Each string is a parameter
	 * of the object.
	 *
	 * @param array $include  List of infoblocks to include
	 * @param array $exclude  (Optional) List of infoblocks to exclude
	 *
	 * @return array
	 */
	public static function getFields($include, $exclude = []) {
		if (count($include) > 1) {
			$fields = array_unique(call_user_func_array('array_merge', $include));
		} else {
			$fields = array_unique($include[0]);
		}

		if (!empty($exclude)) {
			if (count($exclude) > 1) {
				$fields = array_diff($fields, array_unique(call_user_func_array('array_merge', $exclude)));
			} else {
				$fields = array_diff($fields, array_unique($exclude[0]));
			}
		}

		return $fields;
	}

	/**
	 * Checks if the object has all the provided info blocks.
	 *
	 * An info block is an array of arrays of strings. Each string is a parameter
	 * of the object.
	 *
	 * @param array $include  List of infoblocks to include
	 * @param array $exclude  (Optional) List of infoblocks to exclude
	 *
	 * @return array
	 */
	public function getInfo($include, $exclude = []) {
		$output = [];
		$fields = self::getFields($include, $exclude);

		foreach ($fields as $field) {
			// ACHTUNG: if the value is not unidimensional (string, integer, etc)
			// then this is potentially insecure if the programmer is unaware.
			if (array_key_exists($field, $this->data)) { $output[$field] = $this->data[$field]; }
		}

		return $output;
	}

	/**
	 * Checks if the object has all the provided info blocks.
	 *
	 * @param array $include  List of infoblocks to include
	 * @param array $exclude  (Optional) List of infoblocks to exclude
	 *
	 * @return bool
	 */
	public function hasInfo($include, $exclude = []) {
		$fields = self::getFields($include, $exclude);

		foreach ($fields as $field) {
			\Dragonfly\Logger::debug('testing field:' . $field);
			if (!array_key_exists($field, $this->data)) { return false; }
		}

		return true;
	}

	/**
	 * Check the validity of the object's data structure.
	 *
	 * Should only validate the fields which are set making no judgements
	 * regarding object completness. For that use the 'hasInfo' method to assure
	 * object completeness.
	 *
	 * ACHTUNG: must be implemented by all non abstract extensions
	 *
	 * @return bool
	 */
	abstract public function isValid();

}
