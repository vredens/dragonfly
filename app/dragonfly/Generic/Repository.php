<?php

namespace Dragonfly\Generic;

class Repository {

	private $conn;
	private $db;
	private $tbl;

	/**
	 * Must receive a container
	 */
	public function __construct (\Dragonfly\Data\SQLDatabase $db) {
		$this->conn = $db->getConn();
		$this->db = $db;
	}

	/**
	 * Converts PDOException into a RepositoryException and throws it
	 *
	 * @param \PDOException $e
	 * @param string $msg
	 *
	 * @throws \Dragonfly\User\RepositoryException
	 */
	static protected function handlePDOException(\PDOException $e, $msg = 'Repository exception', $code = 500) {
		// log original exception
		\Dragonfly\Logger::error('PDO Exception', ['error_info' => $e->errorInfo, 'error_code' => $e->getCode()]);

		// convert it to something less verbose
		throw new RepositoryException($msg, $code);
	}
}
