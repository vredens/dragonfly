<?php

namespace Dragonfly\Settings;

use \Dragonfly\Data\RepositoryException;

class Repository {

	private $conn;
	private $db;
	private $tbl;

	/**
	 * Must receive a container
	 */
	public function __construct (\Dragonfly\Data\SQLDatabase $db) {
		$this->conn = $db->getConn();
		$this->db = $db;
		$this->tbl = [
			'settings' => $db->generateTableName('settings')
		];
	}

	/**
	 * Converts PDOException into a RepositoryException and throws it
	 *
	 * @param \PDOException $e
	 * @param string $msg
	 *
	 * @throws \Dragonfly\User\RepositoryException
	 */
	static protected function handlePDOException(\PDOException $e, $msg = 'Repository exception', $code = 500) {
		// log original exception
		\Dragonfly\Logger::error('PDO Exception', ['error_info' => $e->errorInfo, 'error_code' => $e->getCode()]);

		// convert it to something less verbose
		throw new RepositoryException($msg, $code);
	}

	/**
	 * Search settings.
	 *
	 * If no filters are provided then all settings are listed.
	 *
	 * @param Array (optional) $filters
	 *
	 * @return Array;
	 * @throws \Dragonfly\Data\RepositoryException;
	 */
	public function search($filters = []) {
		\Dragonfly\Logger::debug('setings::search', ['filters' => $filters]);

		$tn = $this->tbl['settings'];
		$sql = "SELECT * FROM `$tn`;";

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute();
			$settings = $pst->fetchAll();
		} catch (\PDOException $e) {
			self::handlePDOException($e, 'Search settings: FAILED.');
		}

		return $settings;
	}

	/**
	 * Create/update a setting.
	 *
	 * @param String $key
	 * @param String $value
	 *
	 * @throws \Dragonfly\Data\RepositoryException;
	 */
	public function set($key, $value) {
		\Dragonfly\Logger::debug('setings::set', ['key' => $key, 'value' => $value]);

		$tn = $this->tbl['settings'];
		$sql = "REPLACE INTO `$tn` (`key`, `value`) VALUES (:key, :value);";

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute([
				':key' => $key,
				':value' => $value
			]);
			$pst = null;
		} catch (\PDOException $e) {
			self::handlePDOException($e, 'Create/update setting: FAILED.');
		}
	}


	/**
	 * Delete a setting.
	 *
	 * @param String $key
	 *
	 * @throws \Dragonfly\Data\RepositoryException;
	 */
	public function delete($key) {
		\Dragonfly\Logger::debug('setings::delete', ['key' => $key]);

		$tn = $this->tbl['settings'];
		$sql = "DELETE FROM `$tn` WHERE `key` = :key;";

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute([
				':key' => $key
			]);
			$pst = null;
		} catch (\PDOException $e) {
			self::handlePDOException($e, 'Delete setting: FAILED.');
		}
	}
}
