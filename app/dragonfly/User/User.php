<?php

namespace Dragonfly\User;

/**
 * This is a meta-class in the sense that it stores any data structure in the
 * form of a Hash/Dictionary. It contains a validation method which should be
 * used whenever the programmer wants to make sure he has a valid entity. This
 * lets you "construct" the instance iteratively or even add your own temporary
 * properties without conflicts.
 */
class User extends \Dragonfly\Generic\AppObject {
	public static $IDENTITY_FIELDS = ['id'];
	public static $AUTH_FIELDS = ['id', 'user', 'pass', 'enabled'];
	public static $BASIC_FIELDS = ['id', 'user'];
	public static $HIDDEN_FIELDS = ['pass'];
	public static $EXTRA_FIELDS = ['first_name', 'last_name', 'address' ];

	/**
	 * Returns true if the current data structure has no user identification.
	 *
	 * @return bool
	 */
	public function isAnonymous() {
		return !isset($this->data['id']) || empty($this->data['id']);
	}

	/**
	 * Checks if the User data structure is valid.
	 */
	public function isValid() {
		// TODO

		return true;
	}

	/**
	 * Generates a new access token for this user.
	 *
	 * @return string
	 */
	public function generateToken() {
		if (!$this->hasIdentityInfo()) {
			throw new \Exception('Can not generate token without identity info.');
		}

		return md5($this->data['id'] . $this->data['user'] . time());
	}

	/**
	 * Checks if the User data structure has identity information.
	 *
	 * This checks if the user has the following fields:
	 * - id
	 *
	 * @return bool
	 */
	public function hasIdentityInfo() {
		return $this->hasInfo([self::$IDENTITY_FIELDS]);
	}

	/**
	 * Checks if the User data structure has basic information.
	 *
	 * This checks if the user has the following fields:
	 * - id
	 * - nome
	 * - TODO
	 *
	 * @return bool
	 */
	public function hasBasicInfo() {
		return $this->hasInfo([self::$BASIC_FIELDS]);
	}

	/**
	 * Checks if the User data structure has complete information.
	 *
	 * This checks if the user has the following fields:
	 * - id
	 * - nome
	 * - TODO
	 *
	 * @return bool
	 */
	public function hasCompleteInfo() {
		return $this->hasInfo([self::$BASIC_FIELDS, self::$EXTRA_FIELDS]);
	}
}
