<?php

namespace Dragonfly\User;

use \Dragonfly\Generic\RepositoryException;

class Repository {

	private $conn;
	private $db;
	private $tbl;

	/**
	 * Must receive a container
	 */
	public function __construct (\Dragonfly\Data\SQLDatabase $db) {
		$this->conn = $db->getConn();
		$this->db = $db;
		$this->tbl = [
			'user' => $db->generateTableName('user')
		];
	}

	/**
	 * Converts PDOException into a RepositoryException and throws it
	 *
	 * @param \PDOException $e
	 * @param string $msg
	 *
	 * @throws \Dragonfly\User\RepositoryException
	 */
	static protected function handlePDOException(\PDOException $e, $msg = 'Repository exception', $code = 500) {
		// log original exception
		\Dragonfly\Logger::error('PDO Exception', ['error_info' => $e->errorInfo, 'error_code' => $e->getCode()]);

		// convert it to something less verbose
		throw new RepositoryException($msg, $code);
	}

	/**
	 * @param String $login     The username
	 * @param String $password  The MD5 of the textual password
	 *
	 * @return \Dragonfly\User\User;
	 * @throws \Dragonfly\User\RepositoryException;
	 */
	public function getByLogin($login, $password) {
		\Dragonfly\Logger::debug('fetching user', ['login' => $login, 'password' => $password]);

		$sql = 'SELECT *';
		$sql .= ' FROM ' . $this->tbl['user'];
		$sql .= ' WHERE user = :user AND pass = :pass';

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute([':user' => $login, ':pass' => $password]);
			$users = $pst->fetchAll();
		} catch (\PDOException $e) {
			self::handlePDOException($e, 'Failed to check user login');
		}

		if (!empty($users)) {
			return new User($users[0]);
		}

		return new User();
	}

	/**
	 * TODO
	 *
	 * @param String $token     The user's auth token
	 *
	 * @return \Dragonfly\User\User;
	 * @throws \Dragonfly\User\RepositoryException;
	 */
	public function getByToken($token) {
		if (empty($token)) {
			return new User();
		}

		throw new RepositoryException('Not implemented');
	}

	/**
	 * Fetch a user by its id.
	 *
	 * @param String $id      The user's id
	 * @param Array  $include The user fields to include
	 * @param Array  $exclude The user fields to exclude
	 *
	 * @return \Dragonfly\User\User;
	 *
	 * @throws \Dragonfly\User\RepositoryException;
	 */
	public function getById($id, $include = [], $exclude = []) {
		if (empty($include)) { $include = [User::$BASIC_FIELDS]; }

		$fields_str = join(',', User::getFields($include, $exclude));

		$sql = "SELECT $fields_str";
		$sql .= ' FROM ' . $this->tbl['user'];
		$sql .= ' WHERE id = :id';

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute([':id' => $id]);
			$users = $pst->fetchAll();
		} catch (\PDOException $e) {
			self::handlePDOException($e, 'Failed to fetch user by id');
		}

		if (!empty($users)) {
			return new User($users[0]);
		}

		return new User();
	}

	/**
	 * Fill in basic information about the user
	 */
	public function fillBasicInfo(User $user) {
		if (!$user->hasIdentityInfo()) { throw new \RepositoryException('User has no identity information.'); }

		$fields_str = join(',', User::getFields([User::$BASIC_FIELDS]));

		$sql = "SELECT $fields_str";
		$sql .= ' FROM ' . $this->tbl['user'];
		$sql .= ' WHERE id = :id';

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute([':id' => $user->get('id')]);
			$data = $pst->fetch();
		} catch (\PDOException $e) {
			self::handlePDOException($e, 'Failed to fetch user basic info');
		}

		//\Dragonfly\Logger::debug('user:', $data);

		if (empty($data)) {
			throw new RepositoryException('Failed to fill user info');
		}

		$user->set($data);

		return $user;
	}

	/**
	 * Fill in all possible information about the user
	 *
	 * @params \Dragonfly\User\User $user
	 *
	 * @return \Dragonfly\User\User
	 */
	public function fillCompleteInfo(User $user) {
		if (!$user->hasIdentityInfo()) { throw new \RepositoryException('User has no identity information.'); }

		$sql = 'SELECT *';
		$sql .= ' FROM ' . $this->tbl['user'];
		$sql .= ' WHERE id = :id';

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute([':id' => $user->get('id')]);
			$data = $pst->fetch();
		} catch (\PDOException $e) {
			self::handlePDOException($e, 'Failed to fetch user by id');
		}

		//\Dragonfly\Logger::debug('user:', $data);

		if (empty($data)) {
			throw new RepositoryException('Failed to fill user info');
		}

		$user->set($data);

		return $user;
	}
}
