<?php

namespace Dragonfly;

use \Slim\Slim;

abstract class Logger {
	static protected $logger;
	static protected $profiling = [];
	static protected $code = 0;

	static public function set($logger) {
		self::$logger = $logger;
	}

	static public function get() {
		return self::$logger;
	}

	static public function debug($msg, $context = []) {
		if (isset(self::$logger)) {
			self::$logger->debug($msg, $context);
		}
	}

	static public function info($msg, $context = []) {
		if (isset(self::$logger)) {
			self::$logger->info($msg, $context);
		}
	}

	static public function warn($msg, $context = []) {
		if (isset(self::$logger)) {
			self::$logger->warn($msg, $context);
		}
	}

	static public function error($msg, $context = []) {
		if (isset(self::$logger)) {
			self::$logger->error($msg, $context);
		}
	}

	static public function critical($msg, $context = []) {
		if (isset(self::$logger)) {
			self::$logger->critical($msg, $context);
		}
	}

	/**
	 * Starts a profiling block.
	 *
	 * This method returns a code which must be used when calling profEnd method.
	 *
	 * @return string  The profiling block code
	 */
	static public function profStart() {
		$code = self::$code++;

		static::$profiling[$code] = microtime(true);

		return $code;
	}

	/**
	 * Ends a profiling block.
	 *
	 * If a $namespace is passed then a log is printed (as DEBUG level). If a $msg
	 * is passed then it replaces the default profile log message.
	 *
	 * @param string $code  The profiling block code as returned by profStart
	 * @param string $namespace  (Optional) The profiling namespace
	 * @param string $msg  (Optional)
	 *
	 * return float  The profile block elapsed time in miliseconds
	 */
	static public function profEnd($code, $namespace = 'none', $msg = null) {
		// throw exception for invalid profile entries
		if (!isset(self::$profiling[$code])) {
			throw new \Exception('invalid profiling code requested');
		}

		$time = (microtime(true) - static::$profiling[$code]) * 1000;

		if (isset($msg)) {
			self::debug($msg, [ 'ns' => $namespace, 't' => $time]);
		} else {
			self::debug('profiler [ns:'.$namespace.'][t:' . $time . 'ms]', [ 'ns' => $namespace, 't' => $time]);
		}

		unset(self::$profiling[$code]);

		return $time;
	}
}
