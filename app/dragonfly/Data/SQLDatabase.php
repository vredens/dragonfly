<?php

namespace Dragonfly\Data;

class SQLDatabase {
	private $conn;
	private $opts = [];

	public function __construct(\PDO $conn, $opts = []) {
		$this->conn = $conn;
		$this->opts = $opts;
	}

	/**
	 * Returns the database connection.
	 *
	 * @return \PDO
	 */
	public function getConn() {
		return $this->conn;
	}

	/**
	 * Check if a table exists.
	 *
	 * @param string $name The table name
	 * @return bool  Returns true if the table exists, false otherwise
	 * @throws \PDOException
	 */
	public function tableExists($name) {
		$table = $this->generateTableName($name);

		$sql = "SELECT 1 FROM $table LIMIT 1;";

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute();
		} catch (\PDOException $e) {
			// we only expect the code for table does not exist, anything else we
			// throw it forward
			if ($e->getCode() != '42S02') {
				\Dragonfly\Logger::error('PDO Exception', ['error_info' => $e->errorInfo, 'error_code' => $e->getCode()]);
				throw $e;
			}

			return false;
		}

		return true;
	}

	/**
	 * Counts all entries in a table.
	 *
	 */
	public function tableCount($name) {
		$table = $this->generateTableName($name);

		$sql = "SELECT count(*) as entries FROM $table;";

		try {
			$pst = $this->conn->prepare($sql);
			$pst->execute();
			$cnt = $pst->fetch();
		} catch (\PDOException $e) {
			\Dragonfly\Logger::error('PDO Exception', ['error_info' => $e->errorInfo, 'error_code' => $e->getCode()]);
			throw $e;
		}

		return (int)$cnt['entries'];
	}

	/**
	 * Helper script to combine table prefixes with table names when the database
	 * has been configured to use table prefixes.
	 *
	 * @param string $name
	 * @return string
	 */
	public function generateTableName($name) {
		if (isset($this->opts['table-prefix'])) {
			return $this->opts['table-prefix'] . $name;
		}

		return $name;
	}
}
