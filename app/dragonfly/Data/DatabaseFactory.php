<?php

namespace Dragonfly\Data;

abstract class DatabaseFactory {
	protected static $config = [];
	protected static $setup = false;
	protected static $databases = [
		'sql' => [], // store SQL databases here
	];
	protected static $conns = [
		'pdo' => []
	];

	/**
	 * Setup the DatabaseFactory.
	 *
	 * The configuration is a hash like:
	 *
	 * [
	 *   'sql' => [ // the type of database is the main group
	 *     'dbname' => [ // a database name
	 *        // the necessary configuration parameters
	 *        'driver'   => 'mysql',
	 *        'host'     => 'localhost',
	 *        'dbname'   => 'dbname',
	 *        'username' => 'myuser',
	 *        'password' => 'mypass',
	 *     ],
	 *     // database aliases so you can split your code logically instead of
	 *     // directly tied to a database and allowing you to split data across
	 *     // multiple databases if you want.
	 *     'alias' => [
	 *        'use' => 'dbname'
	 *     ]
	 *   ]
	 * ]
	 *
	 * @param array $config
	 */
	public static function setup($config) {
		assert('is_array($config)');

		// set the raw configuration
		self::$config = $config;

		// initialize the database cache
		foreach ($config as $type => $v) {
			self::$databases[$type] = [];
		}

		self::$setup = true;
	}

	protected static function assertSetup() {
		if (! self::$setup) { throw new \Exception('DatabaseFactory is not configured.'); }
	}

	/**
	 * Returns a database configuration.
	 *
	 * @param string $type  The type of database (e.g.: 'sql')
	 * @param string $name  The database name
	 *
	 * @return array
	 * @throws \Exception
	 */
	protected static function getDatabaseConfiguration($type, $name) {

		$code = \Dragonfly\Logger::profStart(); // TODO: remove this

		self::assertSetup();

		$_name = $name;

		if (! isset(self::$config[$type])) { throw new \Exception("No databases configured for type [$type]."); }
		//if (! isset(self::$config[$type][$_name])) { $_name = 'default'; }
		if (! isset(self::$config[$type][$name])) { throw new \Exception('No appropriate database configuration for your request. Please review your configuration.'); }

		$root_config = self::$config[$type][$name];
		if (isset($root_config['use'])) {
			$src_name = $root_config['use'];
			if (! isset(self::$config[$type][$src_name])) { throw new \Exception('Database alias does not exist. Please review your configuration.'); }
			$src_config = self::$config[$type][$src_name];
			if (isset($src_config['use'])) { throw new \Exception('Database "use" chaining is not permitted. Please review your configuration.'); }
			$config = array_merge($src_config, $root_config);
		} else {
			$config = $root_config;
		}

		\Dragonfly\Logger::profEnd($code, 'db::config::parser'); // TODO: remove this

		return $config;
	}

	/**
	 * Returns a PDO Instance for the given connection.
	 *
	 * Cache level indicates how to cache the given connection.
	 * Cache level 0: this disables the use of cache, which means that each call
	 * will create a new PDO connection.
	 * Cache level 1: this cache uses only $name to identify a connection and
	 * returns the same connection for the same $name value
	 * Cache level 2 (default): this cache uses the connection parameters to id
	 * the connection, which means that even connections with different name, as
	 * long as they have the same connection parameters, will be the same the same
	 *
	 * @param string $name   The name of the connection
	 * @param array  $dbc    The database configuration
	 * @param bool   $cache
	 *
	 * @return \PDO
	 */
	public static function getPDOConn($name, $dbc, $cache = 0) {
		assert('!empty($dbc[\'driver\'])');
		assert('!empty($dbc[\'host\'])');
		assert('!empty($dbc[\'dbname\'])');
		assert('!empty($dbc[\'username\'])');
		assert('!empty($dbc[\'password\'])');

		$dsn = $dbc['driver'] . ':host=' . $dbc['host'] . ';dbname=' . $dbc['dbname'];
		$username = $dbc['username'];
		$password = $dbc['password'];

		switch ($cache) {
			case 0:
				$id = null;
			break;

			case 1:
				$id = $name;
			break;

			default:
				$id = md5($dsn . $username . $password);
			break;
		}

		// always returns a new connection when cache disabled or we have a cache miss
		if (empty($id) or !isset(self::$conns['pdo'][$id])) {
			try {
				$dbh = new \PDO($dsn, $username, $password, [
					\PDO::ATTR_PERSISTENT => true,
					\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
					\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
				]);
			} catch (\Exception $e) {
				\Dragonfly\Logger::error($e);

				throw $e;
			}

			// cache the connection if caching is enabled
			if (!empty($id)) {
				self::$conns['pdo'][$id] = $dbh;
			}

			return $dbh;
		}

		// return the cached value
		return self::$conns['pdo'][$id];
	}

	/**
	 * Return a SQL database instance matching the provided name.
	 *
	 * @param string $name     The database you wish to instance
	 *
	 * @return \Dragonfly\Data\SQLDatabase
	 */
	public static function getSQLDatabase($name) {
		if (! self::$setup) { throw new \Exception('DatabaseFactory is not configured.'); }

		// if we have a cache hit we return it
		if (isset(self::$databases['sql'][$name])) { return self::$databases['sql'][$name]; }

		// get the database configuration
		$dbc = self::getDatabaseConfiguration('sql', $name);
		\Dragonfly\Logger::debug('conn ['.$name.']', $dbc);

		// fetch the PDO connection
		$pdo = self::getPDOConn($name, $dbc);

		// store it in the cache
		self::$databases['sql'][$name] = new SQLDatabase($pdo, $dbc);

		return self::$databases['sql'][$name];
	}

}
