<?php

namespace Dragonfly\Data\FileStorage;

use \Dragonfly\Data\FileStorage;

class FileSystemStorage extends FileStorage {
	public function __construct($config) {
		assert('!empty($config[\'path\']);', 'No value configured for path');
		//assert('!empty($config[\'namespace\']', 'No value configured for namespace');

		parent::__construct();

		$this->path = $config['path'];

		// configure headers
		if (empty($config['headers'])) {
			// set default headers
			$this->headers = [
				'Content-Transfer-Encoding' => 'binary',
				'Expires' => date(DATE_RFC2822, time() + 120 * 24 * 60 * 60), // default to 120 days into the future
				'Cache-Control' => 'public'
			];
			//header("Content-Disposition: attachment; filename=\"{$_GET['name']}\" ");
			//header("Content-Disposition: attachment; filename={$_GET['name']}");
			//header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		} else {
			$this->headers = $config['headers'];
		}
	}

	/**
	 * Output file contents.
	 *
	 * Called by the outputViaHTTP function. This method must be implemented and
	 * should simply return file contents without setting any header. A simple
	 * data dump.
	 *
	 * @param Array $filedata The filedata as per what is returned by the 'get' method.
	 */
	protected function outputFileViaHTTP($filedata, $options = []) {
		if (!is_file($filedata['path'])) {
			\Dragonfly\Logger::error('Filedata has path to missing file.', ['filedata' => $filedata]);
			throw new \Dragonfly\API\CustomException('File not found.', \Dragonfly\API\HTTPStatusCodes::NOT_FOUND);
		}

		readfile($filedata['path']);
	}

	/**
	 * Store an uploaded file.
	 *
	 * This method will MOVE the uploaded file. The $filedata['path'] must point
	 * to an uploaded file.
	 *
	 * @param String $namespace
	 * @param String $id
	 * @param Array  $filedata
	 */
	public function storeUploadedFile($namespace, $id, $filedata) {
		// input validator
		self::assertValidFileData($filedata);

		// move the uploaded file
		$dst = $this->path . '/' . $namespace . '-' . $id;
		if (move_uploaded_file($filedata['path'], $dst) === true) {
			//$avatar['url'] = '/user/' . $user_id . '/avatar';
			$filedata['path'] = $dst;
		} else {
			throw new \Exception('Failed to move uploaded file.');
		}

		$this->saveToDatabase($namespace, $id, $filedata);

		return [
			'ns' => $namespace,
			'id' => $id
		];
	}

	/**
	 * Store any file.
	 *
	 * Note that this method will COPY the file.
	 *
	 * @param String $namespace
	 * @param String $id
	 * @param Array  $filedata
	 */
	public function storeFile($namespace, $id, $filedata) {
		// input validator
		self::assertValidFileData($filedata);

		$dst = $this->path . '/' . $namespace . '-' . $id;

		if (!copy($filedata['path'], $dst)) { throw new \Exception('failed to copy [' . $filedata['path'] . '] to [' . $dst . ']'); }

		$filedata['path'] = $dst;

		$this->saveToDatabase($namespace, $id, $filedata);

		return [
			'ns' => $namespace,
			'id' => $id
		];
	}

	/**
	 * Store data into file.
	 *
	 * For the $filedata['path'] use the value 'mem'
	 *
	 * @param String $namespace
	 * @param String $id
	 * @param Array  $filedata
	 */
	public function storeData($namespace, $id, $filedata, $data) {
		// input validator
		self::assertValidFileData($filedata);

		if ($filedata['path'] !== 'mem') {
			throw new \Exception('invalid filedata[\'path\'], expecting \'mem\'');
		}

		$dst = $this->path . '/' . $namespace . '-' . $id;

		file_put_contents($data, $dst);

		$filedata['path'] = $dst;

		$this->saveToDatabase($namespace, $id, $filedata);

		return [
			'ns' => $namespace,
			'id' => $id
		];
	}

	/**
	 * Store an URL contents.
	 *
	 * This will download and store the provided URL. The $filedata['path'] must
	 * be in URL format.
	 *
	 * @param String $namespace
	 * @param String $id
	 * @param Array  $filedata
	 */
	public function storeURL($namespace, $id, $filedata) {
		// input validator
		self::assertValidFileData($filedata);

		$dst = $this->path . '/' . $namespace . '-' . $id;

		// TODO: download $filedata['path'] to $dst
		throw new \Exception('TODO');

		$filedata['path'] = $dst;

		$this->saveToDatabase($namespace, $id, $filedata);

		return [
			'ns' => $namespace,
			'id' => $id
		];
	}

	/**
	 * Fetch filedata.
	 *
	 * @param String $namespace
	 * @param String $id
	 *
	 * @return Array File data structure
	 */
	public function get($namespace, $id) {
		$data = $this->getFromDatabase($namespace, $id);

		return $data;
	}
}
