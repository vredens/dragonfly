<?php

namespace Dragonfly\Data;

use \Dragonfly\Data\FileStorage\FileSystemStorage;
use \Dragonfly\Logger;

class FileStorageFactory {
	protected static $config;
	protected static $setup = false;
	protected static $instances = [];

	public static function setup($config) {
		self::$config = $config;

		self::$setup = true;
	}

	public static function checkSetup($name = null, $exceptionOnError = true) {
		if (isset($name)) {
			if (empty(self::$config[$name])) { throw new \Exception("Bad storage configuration: no configuration set for [$name]"); }

			$testcase = [
				$name => self::$config[$name]
			];
		} else {
			$testcase = self::$config;
		}

		foreach (array_keys($testcase) as $n) {
			$c = self::getFileStorageConfig($n);

			if (empty($c['type'])) { throw new \Exception("Bad storage configuration: [$n.type] is required"); }

			switch ($c['type']) {
				case 'filesystem':
					if (empty($c['path'])) { throw new \Exception("Bad storage configuration: [$n.path] is required"); }
					if (!is_dir($c['path'])) { throw new \Exception("Bad storage configuration: [$n.path:{$c['path']}] must be a folder"); }
					if (!is_writable($c['path'])) { throw new \Exception("Bad storage configuration: [$n.path:{$c['path']}] must be writable by [" . get_current_user() . ']'); }
				break;

				default:
					throw new \Exception("Bad storage configuration: [$n.type:{$c['type']}] value is not recognized]");
				break;
			}
		}
	}

	protected static function assertSetup() {
		if (! self::$setup) { throw new \Exception('DatabaseFactory is not configured.'); }
	}

	protected static function getFileStorageConfig($name) {

		$code = Logger::profStart(); // TODO: remove this

		self::assertSetup();

		$_name = $name;

		if (! isset(self::$config[$name])) { throw new \Exception("No storage system configured with name [$name]."); }

		$root_config = self::$config[$name];
		if (isset($root_config['use'])) {
			$src_name = $root_config['use'];
			if (! isset(self::$config[$src_name])) { throw new \Exception('Storage system alias does not exist. Please review your configuration.'); }
			$src_config = self::$config[$src_name];
			if (isset($src_config['use'])) { throw new \Exception('Storage system "use" chaining is not permitted. Please review your configuration.'); }
			$config = array_merge($src_config, $root_config);
		} else {
			$config = $root_config;
		}

		Logger::profEnd($code, 'FileStorageFactory::config::parser'); // TODO: remove this

		return $config;
	}

	public static function getFileStorage($name) {
		// return from cache
		if (isset(self::$instances[$name])) {
			return self::$instances[$name];
		}

		$config = self::getFileStorageConfig($name);

		assert('!empty($config[\'type\']);', 'file storage configuration has no value for "type"');

		switch ($config['type']) {
			case 'filesystem':
				self::$instances[$name] = new FileSystemStorage($config);
			break;

			default:
				throw new \Exception('invalid file storage type [' . $config['type'] . ']');
			break;
		}

		return self::$instances[$name];
	}
}
