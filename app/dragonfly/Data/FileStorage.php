<?php

namespace Dragonfly\Data;

use \Dragonfly\Data\DatabaseFactory;

abstract class FileStorage {
	/**
	 * FileStorage constructor
	 *
	 * @param Array $config Configuration specific to the underlying storage system
	 */
	public function __construct($config = []) {
		$this->db = $db = DatabaseFactory::getSQLDatabase('storage');

		$this->table = $tn = $db->generateTableName('files');
		if (! $db->tableExists('files')) {
			// CREATE TABLE
			$sql = "
			CREATE TABLE `$tn` (
				`namespace` VARCHAR(64) NOT NULL,
				`id` VARCHAR(256) NOT NULL,
				`data` TEXT,
				`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`namespace`, `id`)
			) DEFAULT CHARSET=latin1;
			";

			$dbh = $db->getConn();
			$pst = $dbh->prepare($sql);
			$pst->execute();
			$pst = null;
		}
	}

	protected function saveToDatabase($namespace, $id, $data) {
		try {
			$tn = $this->table;
			$dbh = $this->db->getConn();
			$pst = $dbh->prepare("REPLACE INTO `$tn` (`namespace`, `id`, `data`) VALUES (:namespace, :id, :data);");
			$pst->execute([
				':namespace' => $namespace,
				':id' => $id,
				':data' => json_encode($data)
			]);
			//$pst->fetchAll();
			$pst = null;
		} catch (\PDOException $e) {
			// log original exception
			\Dragonfly\Logger::error('PDO Exception', ['error_info' => $e->errorInfo, 'error_code' => $e->getCode()]);

			// convert it to something less verbose
			throw new \Exception('Failed to save data to database');
		}
	}

	protected function getFromDatabase($namespace, $id) {
		try {
			$tn = $this->table;
			$dbh = $this->db->getConn();
			$pst = $dbh->prepare("SELECT * FROM `$tn` WHERE `namespace` = :namespace AND `id` = :id;");
			$pst->execute([
				':namespace' => $namespace,
				':id' => $id
			]);
			$data = $pst->fetchAll();
			$pst = null;
		} catch (\PDOException $e) {
			// log original exception
			\Dragonfly\Logger::error('PDO Exception', ['error_info' => $e->errorInfo, 'error_code' => $e->getCode()]);

			// convert it to something less verbose
			throw new \Exception('Failed to fetch data');
		}

		// TODO: add checks to make sure we can do this
		return json_decode($data[0]['data'], true);
	}

	/**
	 * Method which validates a valid filedata data structure.
	 *
	 * @param mixed $filedata
	 */
	public static function assertValidFileData($filedata) {
		assert('is_array($filedata);', '$filedata is not an array');
		assert('!empty($filedata[\'name\']);', '$filedata has no value for name');
		assert('!empty($filedata[\'path\']);', '$filedata has no value for path');
		assert('!empty($filedata[\'type\']);', '$filedata has no value for type');
		assert('!empty($filedata[\'size\']);', '$filedata has no value for size');
	}

	/**
	 * Helper function which returns an HTTP response with the file setting all
	 * the necessary HTTP Headers (per what is configured)
	 *
	 * Available options
	 * - 'attachment' => true // makes use of content disposition http header
	 *
	 * @param Array $filedata The filedata as per what is returned by the 'get' method.
	 * @param Array $options  See above for the list of options, defaults to []
	 */
	public function outputViaHTTP($filedata, $options = []) {
		self::assertValidFileData($filedata);

		header("Content-length: " . $filedata['size']);
		header('Content-type: ' . $filedata['type']);

		if (!empty($options)) {
			foreach ($options as $k => $v) {
				switch ($k) {
					case 'attachment':
						if ($v) { header('Content-disposition: attachment; filename="' . $filedata['name'] . '"'); }
					break;
				}
			}
		}

		\Dragonfly\Logger::debug('FileStorage::outputViaHTTPsettings: setting headers', ['headers' => $this->headers]);
		foreach ($this->headers as $k => $v) {
			header("$k: $v");
		}

		// add these two lines
		ob_clean();   // discard any data in the output buffer (if possible)
		flush();      // flush headers (if possible)

		$this->outputFileViaHTTP($filedata);
	}

	/**
	 * Output file contents.
	 *
	 * Called by the outputViaHTTP function. This method must be implemented and
	 * should simply return file contents without setting any header. A simple
	 * data dump.
	 *
	 * @param Array $filedata The filedata as per what is returned by the 'get' method.
	 */
	abstract protected function outputFileViaHTTP($filedata);

	/**
	 * Store an uploaded file.
	 *
	 * This method will MOVE the uploaded file. The $filedata['path'] must point
	 * to an uploaded file.
	 *
	 * @param String $namespace
	 * @param String $id
	 * @param Array  $filedata
	 */
	abstract public function storeUploadedFile($namespace, $id, $filedata);

	/**
	 * Store any file.
	 *
	 * Note that this method will COPY the file.
	 *
	 * @param String $namespace
	 * @param String $id
	 * @param Array  $filedata
	 */
	abstract public function storeFile($namespace, $id, $filedata);

	/**
	 * Store data into file.
	 *
	 * For the $filedata['path'] use the value 'mem'
	 *
	 * @param String $namespace
	 * @param String $id
	 * @param Array  $filedata
	 * @param String $data      File contents
	 */
	abstract public function storeData($namespace, $id, $filedata, $data);

	/**
	 * Store an URL contents.
	 *
	 * This will download and store the provided URL. The $filedata['path'] must
	 * be in URL format.
	 *
	 * @param String $namespace
	 * @param String $id
	 * @param Array  $filedata
	 */
	abstract public function storeURL($namespace, $id, $filedata);

	/**
	 * Fetch filedata.
	 *
	 * @param String $namespace
	 * @param String $id
	 *
	 * @return Array File data structure
	 */
	abstract public function get($namespace, $id);
}
