<?php

namespace Dragonfly;

use \Slim\Slim;

abstract class API {
	private static $output = 'JSON';

	public static function setOutputToJSON() {
		self::$output = 'JSON';
	}

	public static function setOutputToXML() {
		throw new \Exception('XML output format is currently unsupported');
	}

	public static function assertAdminToken() {
		$app = Slim::getInstance();
		$config = $app->container->get('config');

		// try to find the access token as the HTTP Header
		$token = $app->request->headers->get('X-Auth-Token');
		// fallback to using query parameters
		if (empty($token)) {
			$token  = $app->request->params('token');
		}

		// validate admin token
		if ($token !== $config['app.admin.password']) {
			throw new \Dragonfly\API\CustomException('Unauthorized.', \Dragonfly\API\HTTPStatusCodes::UNAUTHORIZED);
		}
	}

	/**
	 * Checks a prepared upload data structure agains several options and any
	 * upload errors. If anything is wrong an exception is thrown.
	 *
	 * @param Array $uploadEntry  The upload entry
	 * @param Int   $error        The upload error code
	 * @param Array $opts         Validation options
	 *
	 * @return Array the final upload entry
	 * @throws \Dragonfly\API\CustomException
	 */
	protected static function validateUpload($entry, $error, $opts) {
		/**
		 * Handle file upload errors.
		 */
		if (!empty($error)) {
			switch ($error) {
				case UPLOAD_ERR_INI_SIZE:
					$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
					throw new \Dragonfly\API\CustomException($message, \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
					break;
				case UPLOAD_ERR_FORM_SIZE:
					$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
					throw new \Dragonfly\API\CustomException($message, \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
					break;
				case UPLOAD_ERR_PARTIAL:
					$message = "The uploaded file was only partially uploaded";
					throw new \Dragonfly\API\CustomException($message, \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
					break;
				case UPLOAD_ERR_NO_FILE:
					$message = "No file was uploaded";
					throw new \Dragonfly\API\CustomException($message, \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
					break;
				case UPLOAD_ERR_NO_TMP_DIR:
					$message = "Missing a temporary folder";
					//throw new \Dragonfly\API\CustomException($message, \Dragonfly\API\HTTPStatusCodes::INTERNAL_SERVER_ERROR);
					throw new \Exception($message);
					break;
				case UPLOAD_ERR_CANT_WRITE:
					$message = "Failed to write file to disk";
					//throw new \Dragonfly\API\CustomException($message, \Dragonfly\API\HTTPStatusCodes::INTERNAL_SERVER_ERROR);
					throw new \Exception($message);
					break;
				case UPLOAD_ERR_EXTENSION:
					$message = "File upload stopped by extension";
					//throw new \Dragonfly\API\CustomException($message, \Dragonfly\API\HTTPStatusCodes::INTERNAL_SERVER_ERROR);
					throw new \Exception($message);
					break;
				default:
					$message = "Unknown upload error";
					//throw new \Dragonfly\API\CustomException($message, \Dragonfly\API\HTTPStatusCodes::INTERNAL_SERVER_ERROR);
					throw new \Exception($message);
					break;
			}
		}

		// TODO: expect the following fields to be set: 'name', 'path', 'size', 'type'

		/**
		 * README: this check exists due to some hosting services disabling this
		 * function due to security issues.
		 * Checking the mime content type this way is actually unimportant unless
		 * there is some post processing of the uploaded file which could result in
		 * exploits on other systems. If it is simply to store and retrieve then the
		 * mime type provided by the caller is more than enough.
		 */
		if(function_exists('mime_content_type')) {
			if ($entry['type'] !== mime_content_type($entry['path'])) {
				throw new \Dragonfly\API\CustomException('Provided mime type does not match file', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
			}
		}

		/**
		 * Check the file's mime-type against the supported mime-types, if such a
		 * list was provided in the options.
		 */
		if (!empty($opts['supported-mime-types'])) {
			$valid = false;
			$i = 0;
			while(!$valid and $i < count($opts['supported-mime-types'])) {
				if ($entry ['type'] === $opts['supported-mime-types'][$i++]) {
					$valid = true;
				}
			}

			if (!$valid) {
				throw new \Dragonfly\API\CustomException('File type is not allowed', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
			}
		}

		return $entry;
	}

	/**
	 * This is a helper method to assist with processing uploads made by using
	 * multipart/form-data requests. You need to provide the form parameter which
	 * should contain the file upload(s). This method is capable of handling multi
	 * file uploads, which requires your client's form to have the parameter name
	 * sufixed with '[]'.
	 *
	 * Options:
	 *   - optional: boolean value indicating if the upload is optional or not
	 *   - supported-mime-types: an array of supported mime types
	 *   - multi: will accept multiple file uploads for the same parameter,
	 *     otherwise, if multiple files have been uploaded then an exception is
	 *     thrown
	 *
	 * @param String $parameter The form's parameter name containing the uploaded file(s)
	 * @param Array  $opts      (optional) Options for handling the file upload
	 *
	 * @return Array A data structure containing the uploaded information
	 * @throws \Dragonfly\API\CustomException
	 */
	public static function processFormUpload($parameter, $opts = []) {
		$app = Slim::getInstance();
		$cfg = $app->container->get('config');

		if (!isset($_FILES[$parameter])) {
			if (empty($opts['optional'])) {
				throw new \Dragonfly\API\CustomException('Bad request', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
			}

			// return an empty object
			return [];
		}

		$files = $_FILES[$parameter];
		$data = [];

		// TODO: allow passing a "{$parameter}_meta" with metadata which is merged with the file upload data

		if (is_array($files['name'])) {
			// caller is not expecting a multi file upload, clearly a bad request
			if (!$opts['multi']) {
				throw new \Dragonfly\API\CustomException('Multi-file upload not allowed for field [' . $parameter . ']', \Dragonfly\API\HTTPStatusCodes::BAD_REQUEST);
			}
			//
			$cnt = count($files['name']);
			for($i = 0 ; $i < $cnt ; $i++) {
				$entry = [
					'name' => $files['name'][$i],
					'type' => $files['type'][$i],
					'size' => $files['size'][$i],
					'path' => $files['tmp_name'][$i],
				];

				$data[] = self::validateUpload($entry, $files['error'][$i], $opts);
			}
		} else {
			$entry = [
				'name' => $files['name'],
				'type' => $files['type'],
				'size' => $files['size'],
				'path' => $files['tmp_name'],
			];

			if (!$opts['multi']) {
				$data = self::validateUpload($entry, $files['error'], $opts);
			} else {
				$data[] = self::validateUpload($entry, $files['error'], $opts);
			}
		}

		return $data;
	}

	/**
	 * Issue an API reply to the client. Make sure you don't call this method
	 * more than once for each request.
	 *
	 * @param array   $body        A data structure already in array format
	 * @param integer $statusCode  An HTTP Statuc Code
	 */
	public static function reply($body, $statusCode = 200) {
		$app = Slim::getInstance();

		$output = [
			's' => $statusCode,
			'd' => $body,
		];

		$response = $app->response;
		$response->setStatus($output['s']);
		$response->headers->set('Content-Type', 'application/json');
		$response->body(json_encode($output));
	}

	/**
	 * Exception handler for using as argument to \Slim\Slim::error method.
	 *
	 * It is recommended to use \Dragonfly\API\Exception (or an extension) in order to
	 * control status codes on your replies since anything else will always result
	 * in a status code of 500 (Internal Server Error)
	 *
	 * @param \Exception $e
	 */
	public static function handleException(\Exception $e) {
		$app = Slim::getInstance();

		if ($e instanceof \Dragonfly\API\CustomException) {
			$body = [
				's' => $e->getCode(),
				'e' => [
					'c' => $e->getCode(),
					'm' => $e->getMessage(),
				]
			];
		} else {
			// Catch and log non API exceptions and send a default error back to the client
			$app->log->error('Unexpected error:' . $e);
			$body = [
				's' => \Dragonfly\API\HTTPStatusCodes::INTERNAL_SERVER_ERROR,
				'e' => [
					'c' => $e->getCode(),
					'm' => 'Internal server error',
				]
			];
		}
		$response = $app->response;
		$response->setStatus($body['s']);
		$response->headers->set('Content-Type', 'application/json');
		$response->body(json_encode($body));
	}
}
