<?php

function read_csv_file($filepath, $opts = []) {
	$opts = array_merge([
		'exclude_first_line' => false,
		'max_buffer_len' => 8096,
		'handler' => null
	], $opts);

	$handle = fopen($filepath, "r");

	if ($handle === false) {
		throw new \Exception("Failed to open file [$filepath]");
	}

	$data = [];

	$first = true;
	while (($line = fgetcsv($handle, $opts['max_buffer_len'], ";")) !== FALSE) {
		// strip first line
		if ($opts['exclude_first_line'] and $first) { $first = 0; continue; }

		if (isset($opts['handler'])) {
			$line = call_user_func_array($opts['handler'], [$line]);
		}

		if (isset($line)) {
			array_push($data, $line);
		}
	}
	fclose($handle);

	return $data;
}
