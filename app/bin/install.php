<?php

/**
 * This is the CLI for installing a dragonfly based application. For sensitive
 * apps or complex installation operations it is recommended to never use the
 * web installation script but rather a command line one.
 */

// TODO: this should not be necessary, it is here as hack to prevent bad php.ini configurations
date_default_timezone_set('Europe/Lisbon');

// TODO: Set this to your application's absolute folder
$app_folder = dirname(__DIR__);

$APPLICATION_MODE='production';

/**
 * Boot the Application
 */

require $app_folder . '/vendor/autoload.php';
require $app_folder . '/bin/database.php';

// parse input as a GET request
parse_str(implode('&', array_slice($argv, 1)), $params);

if (!isset($params['config-file'])) {
	$params['config-file'] = $app_folder . '/config/config.php';
}

db_install($params);
