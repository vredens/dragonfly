<?php

global $config;

$userdb = \Dragonfly\Data\DatabaseFactory::getSQLDatabase('user');
$maindb = \Dragonfly\Data\DatabaseFactory::getSQLDatabase('default');

$tables = [
	'user' => $userdb->generateTableName('user'),
	'settings' => $maindb->generateTableName('settings'),
];

printl("INSTALLING DATABASE");

/**
 * Create table: settings
 *
 * This method constructs the SQL query inline.
 */

printl('  table [', $maindb->generateTableName('settings'), "]");
if (! $maindb->tableExists('settings')) {
	printl("  ... creating");
	$tn = $maindb->generateTableName('settings');
	// CREATE TABLE
	$sql = "
	CREATE TABLE `$tn` (
		`key` VARCHAR(255) NOT NULL PRIMARY KEY,
		`value` VARCHAR(2000) DEFAULT NULL,
		`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
	) DEFAULT CHARSET=latin1;
	";

	$dbh = $maindb->getConn();
	$pst = $dbh->prepare($sql);
	$pst->execute();
	$pst = null;
}
printl("  ... OK");

/**
 * Populate table: settings
 */

if ($maindb->tableCount('settings') == 0) {
	printl("  ... Table is empty, populating with initial data");

	$tn = $maindb->generateTableName('settings');
	// populate initial data
	$sql = "INSERT INTO `$tn` (`key`, `value`) VALUES ";
	$sql .= "('database.version', '0.1.0');";
	// run inserts
	$dbh = $userdb->getConn();
	$pst = $dbh->prepare($sql);
	$pst->execute();
	$pst = null;

	printl("  ... OK");
}

/**
 * Create table: user
 */

printl('  table [', $userdb->generateTableName('user'), "]");
if (! $userdb->tableExists('user')) {
	printl("  ... creating");
	$tn = $userdb->generateTableName('user');
	// CREATE TABLE
	/**
	 * ACHTUNG: using timestamps which means the server internal clock better be
	 * a valid UTC timestamp and synchronized using NTP or other equally accurate
	 * system.
	 */
	$sql = "
	CREATE TABLE `$tn` (
		`id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
		`updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		`user` VARCHAR(255) DEFAULT NULL,
		`pass` VARCHAR(255) DEFAULT NULL,
		`name` VARCHAR(255) DEFAULT NULL,
		`enabled` BOOLEAN DEFAULT TRUE
	) DEFAULT CHARSET=latin1;
	";

	$dbh = $userdb->getConn();
	$pst = $dbh->prepare($sql);
	$pst->execute();
	$pst = null;
}
printl("  ... OK");

/**
 * Populate table: user
 */

if ($userdb->tableCount('user') == 0) {
	printl("  ... Table is empty, populating with initial data");

	$tn = $userdb->generateTableName('user');
	// populate initial data
	$sql = "INSERT INTO `$tn` (`id`, `user`, `pass`) VALUES";
	$sql .= " (1, 'root', '" . md5($config['app.user.root.password']) . "');";
	// run inserts
	$dbh = $userdb->getConn();
	$pst = $dbh->prepare($sql);
	$pst->execute();
	$pst = null;

	printl("  ... OK");
}
