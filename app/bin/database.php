<?php

/*******************************************************************************
 * Helper Functions
 ******************************************************************************/

/**
 * Helper function to print with line ends
 */
function printl() {
	$args = func_get_args();

	echo join('', $args), "\n";
}

/**
 * Helper method to fetch current database version.
 *
 * Exists with positive error code when database has not been installed.
 *
 * @return string The database version number
 */
function get_current_db_version() {
	$maindb = \Dragonfly\Data\DatabaseFactory::getSQLDatabase('default');

	// asser the database has been installed
	if (! $maindb->tableExists('settings')) {
		printl("  ... Does not exist. You need to install the database first");
		printl("  ... NOK");

		exit(1);
	}

	$tn = $maindb->generateTableName('settings');
	$dbh = $maindb->getConn();
	$pst = $dbh->query("SELECT value FROM `$tn` WHERE `key` = 'database.version';", \PDO::FETCH_ASSOC);
	$res = $pst->fetchAll();

	return $res[0]['value'];
}

/**
 * Update current database version to the next final version
 *
 * @param array $dvg   The database version graph
 *
 * @return string  Current database version after all upgrades
 */
function __db_upgrade($dvg) {
	$curr_v = get_current_db_version();

	if (!isset($dvg[$curr_v])) {
		printl('Unexpected Database Version [version:', $curr_v, ']');
		exit(1);
	}

	// We keep running upgrade scripts until we reach a final version. This
	// allows you to have two meta versions so that if the database is in a
	// very old version it will only be upgraded to a certain point.
	while($dvg[$curr_v] != '_final') {
		$next_v = $dvg[$curr_v];
		$script = __DIR__ . '/v' . $next_v . '/upgrade-from-v' . $curr_v . '.php';

		if (!file_exists($script)) {
			printl('Script [', $script, '] does not exist.');
			exit(1);
		}

		require $script;

		$curr_v = get_current_db_version();
	}

	return $curr_v;
}

/**
 * Check all SQL databases
 *
 * @param array $config Database configurations, indexed by the DB name
 *
 * @return bool
 */
function check_sql_databases($config) {
	foreach ($config as $n => $c) {
		try {
			$db = \Dragonfly\Data\DatabaseFactory::getSQLDatabase($n);
			printl("Database [$n]: OK");
		} catch (\Exception $e) {
			printl("Database [$n]: NOK. " . $e->getMessage());
		}
	}
}

/**
 * Check a list of folders.
 *
 * Options:
 * - 'writable' => true|false
 * - 'auto-create' => true|false
 *
 * @param array $folders The list of folders
 * @param array $opts    Options (default: [])
 */
function check_folders ($folders, $opts = []) {
	for($i = 0; $i < count($folders); $i++) {
		$path = $folders[$i];

		printl('Checking folder [' . $path . '] ...');

		if (!is_dir($path)) {
			if ($opts['auto-create']) {
				printl('... does not exist, creating...');
				if (!mkdir($path, 0770, true)) {
					printl('... NOK');
				} else {
					printl('... AOK');
				}
			} else {
				printl('... NOK (does not exist)');
			}
		} else if ($opts['writable'] and !is_writable($path)) {
			printl('... NOK (has no write permissions)');
		} else {
			printl('... AOK');
		}
	}
}

/*******************************************************************************
 * Database Version Graph
 ******************************************************************************/
$database_version_graph = [
	'0.1.0' => '0.2.0', // this will run the script: v0.2.0/upgrade-from-0.1.0.php
	'0.2.0' => '_final'
];

// This should be set to the latest version which has an install script.
$DATABASE_INSTALL_STABLE_VERSION = '0.1.0';

/*******************************************************************************
 * Main install function
 ******************************************************************************/
function db_install($params) {
	global $database_version_graph;
	global $DATABASE_INSTALL_STABLE_VERSION;

	// load the configuration
	// TODO: test if the file exists
	if (!isset($params['config-file'])) {
		throw new \Exception('no parameter [config-file] provided');
	}
	$config = require $params['config-file'];

	if (empty($params['a'])) {
		throw new \Exception('no parameter [a] provided');
	}

	if (empty($params['t']) or empty($config['app.admin.password']) or ($params['t'] !== $config['app.admin.password'])) {
		print 'Powered by Dragonfly';

		return 0;
	}

	if (isset($params['username'])) {
		$config['db.default.username'] = $params['username'];
	}

	if (isset($params['password'])) {
		$config['db.default.password'] = $params['password'];
	}

	$action = $params['a'];

	\Dragonfly\Data\DatabaseFactory::setup($config['databases']);

	switch($action) {
		/**
		 * The check action should be used to make sure your system is properly
		 * setup to run the application. Should also check communication with
		 * external resources and API.
		 */
		case 'check':
			$databases = $config['databases'];

			// check all SQL databases
			if (!empty($databases)) {
				if (!empty($databases['sql'])) {
					check_sql_databases($databases['sql']);
				}

				// TODO: fill in other database checks
			}

			// storage engine checks
			if (!empty($config['storage'])) {
				\Dragonfly\Data\FileStorageFactory::setup($config['storage']);

				try {
					\Dragonfly\Data\FileStorageFactory::checkSetup();
					printl('Storage engine: AOK');
				} catch (\Exception $e) {
					printl('Storage engine: NOK. ' . $e->getMessage());
				}
			}

			// list of app folders to check for read-write permissions
			$app_rw_folders = [
				dirname($config['slim.log.filename']),
				dirname($config['app.log.filename']),
				$config['app.templates.cache'],
			];
			if (!empty($config['app.session.path'])) {
				array_push($app_rw_folders, $config['app.session.path']);
			}

			// check if all read-write folders exist and are read-writable
			check_folders($app_rw_folders, [
				'auto-create' => isset($config['app.mkdir']) ? $config['app.mkdir'] : false,
				'writable' => true,
			]);

		break;

		case 'install':
			require __DIR__ . '/v' . $DATABASE_INSTALL_STABLE_VERSION . '/install.php';
			$curr_v = __db_upgrade($database_version_graph);
			printl('Database is in final version [', $curr_v, '].');
		break;

		case 'update':
		case 'upgrade':
			$curr_v = __db_upgrade($database_version_graph);
			printl('Database is in final version [', $curr_v, '].');
		break;

		default:
			printl('Bad operation. Valid actions are `install`, `upgrade`, `check`');
		break;
	}
}
