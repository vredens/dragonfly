<?php

global $config;

$userdb = \Dragonfly\Data\DatabaseFactory::getSQLDatabase('user');
$maindb = \Dragonfly\Data\DatabaseFactory::getSQLDatabase('default');

$tables = [
	'user' => $userdb->generateTableName('user'),
	'settings' => $maindb->generateTableName('settings'),
];

printl("UPGRADING DATABASE [v0.1.0] to [v0.2.0]");

/**
 * Create table: settings
 */

printl('  table [', $maindb->generateTableName('settings'), "]");
if (! $maindb->tableExists('settings')) {
	printl("  ... Does not exist. You need to install the database first");
	printl("  ... NOK");

	exit(1);
} else {
	$v = get_current_db_version();
	//$tn = $maindb->generateTableName('settings');
	//$dbh = $maindb->getConn();
	//$res = $dbh->query("SELECT value FROM `$tn` WHERE `key` = 'database.version';", \PDO::FETCH_ASSOC);

	if ($v != '0.1.0') {
		printl("  ... Wrong Database Version, expecting [0.1.0] found [", $v, "]");

		exit(1);
	}
}
printl("  ... OK");

printl('  table [', $userdb->generateTableName('user'), "]");
$tn = $userdb->generateTableName('user');
$sql = "ALTER TABLE `$tn` ADD UNIQUE KEY (`user`);";
printl("  ... Running: $sql");
$dbh = $userdb->getConn();
$dbh->exec($sql);
printl("  ... OK");

/**
 * At the end of the upgrade script, we setup the new database version
 */

printl("  Updating database version to [0.2.0]");

$tn = $maindb->generateTableName('settings');
// populate initial data
$sql = "UPDATE `$tn` SET `value`='0.2.0' WHERE `key`='database.version';";
// run inserts
$dbh = $maindb->getConn();
if ($dbh->exec($sql) != 1) {
	printl("  ... Something went wrong");
} else {
	printl("  ... OK");
}
