# Dragonfly

## Info

Dragonfly is a fully working PHP Web Application highly oriented for serving a REST API while relying on a single page (or close to that) frontend based on JavaScript and client side dynamic content.

**Main Features**

  * Database
    * installation scripts
    * versioned database
    * update scripts which use iterative updates
  * Centralized Database Connection Factory
  * Centralized Repository Factory
  * Application decoupled from Web-service

## Concepts

### Iterative Updates

**TODO**

### Data Manipulation Objects (DMO)

**TODO**

---

# DEV Environment, a quick start

This is a quick start for setting up your development environment to start working right away. To deploy to a production or QA environment it is recommended to read through all the boot up scripts and configuration files and the _Deployment Instructions_ section of this README.

**Requirements**

  * [Docker](https://docs.docker.com/engine/installation/)
  * PHP 5.6 or later
  * [Composer](<http://getcomposer.org/doc/00-intro.md#installation>)

## Start the containers

```
./do.sh dev start
```

Alternatively you can use composer to do the same

```
composer run dev-start
```

This should launch your application in a docker container and you can access the web application via (http://localhost:8000).

## Setup the database

You can do this using the CLI or the Web interface. If you haven't changed your admin password, which you should, you can simply open the following URL in your browser (http://localhost:8000/install.php?a=install&t=randompasswordiuseeverywhere).

## Logs

To see the application logs run `./do.sh dev logs` or `composer run dev-logs`. You can also check on PHP fatal errors or other web server errors using `docker logs dragonfly-nginx`.

# Deployment Instructions

## Configuration

Use `config/config.php.dist` as template and create the following files

  * [ ] `config/config.php`, which is your application's runtime configuration file (this user should only have `INSERT`, `UPDATE` and `SELECT` permissions)
  * [ ] `config/config.admin.php`, which is your administration configuration file and will be used to run database installation/upgrade scripts or other administrative scripts (this user should have `ALL` permissions)

## Install

**Requirements**

  * [Docker](https://docs.docker.com/engine/installation/)
  * PHP 5.4 or later
  * [Composer](<http://getcomposer.org/doc/00-intro.md#installation>)

**Install dependencies**

```
composer install
```

**Install the database** (requires Mysql 5.6 or later, tested with 5.6.29 and 5.7)

```
./do.sh database install

# or instead of the line above you may run
composer run dbinstall
```

You can also install the database via web, which requires you to deploy the application first. Once the application is deployed simply request (http://yourhost/install.php?a=install&t=youradminpassword) which will perform all necessary updates to your database.

## Deploy

At this point you can deploy the following folders into your web server

  * `app`, this folder should not be web-accessible
  * `public`, this folder should contain all web accessible assets and scripts
