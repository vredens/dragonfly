#!/bin/bash

basedir="$(dirname $(perl -e 'use Cwd qw/abs_path/; print abs_path($ARGV[0]);' $0))"

cd $basedir

#===============================================================================
function __mysql_create {
#===============================================================================
	if ! docker ps -a | grep -q dragonfly-mysql; then
		echo "Creating a new MySQL docker container"
		docker create \
			--name dragonfly-mysql \
			-e MYSQL_ROOT_PASSWORD=12345 \
			-e MYSQL_DATABASE='dragonfly' \
			-e MYSQL_USER='dragonfly' \
			-e MYSQL_PASSWORD='dragonfly' \
			-p 127.0.0.1:3306:3306 \
			mysql:5.6
		echo "Starting the new MySQL docker container"
		docker start dragonfly-mysql
		echo "Waiting 30 seconds for MySQL to boot"
		sleep 30
	fi
}

#===============================================================================
function __mysql_start {
#===============================================================================
	__mysql_create
	if ! docker ps | grep -q dragonfly-mysql; then
		echo "Starting the MySQL docker container"
		docker start dragonfly-mysql
	fi
}

#===============================================================================
function __mysql_stop {
#===============================================================================
	if docker ps | grep -q dragonfly-mysql; then
		echo "Stopping the MySQL docker container"
		docker stop dragonfly-mysql
	fi
}

#===============================================================================
function __mysql_destroy {
#===============================================================================
	__mysql_stop
	if docker ps -a | grep -q dragonfly-mysql; then
		echo "Removing the MySQL docker container"
		docker rm -v dragonfly-mysql
	fi
}

#===============================================================================
function __database_check {
#===============================================================================
	pushd app
	php bin/install.php config-file=config/config.admin.php op=check
	popd
}

#===============================================================================
function __database_install {
#===============================================================================
	pushd app
	php bin/install.php config-file=config/config.admin.php op=install
	popd
}

#===============================================================================
function __database_upgrade {
#===============================================================================
	pushd app
	php bin/install.php config-file=config/config.admin.php op=upgrade
	popd
}

#===============================================================================
function __install_deps {
#===============================================================================
	if [ ! -d app/vendor ]; then
		composer install
	fi
}

#===============================================================================
function __update_deps {
#===============================================================================
	if [ ! -d app/vendor ]; then
		__install_deps
	else
		composer update
	fi
}

#===============================================================================
function __remove_deps {
#===============================================================================
	if [ -d app/vendor ]; then
		rm -rf app/vendor
	fi
}

#===============================================================================
function __web_start_nodocker {
#===============================================================================
	# TODO: convert to a docker container
	if [ ! -d .run/php-sessions ]; then
		mkdir -p .run/php-sessions
	fi
	if [ -d .run/logs ]; then
		> .run/logs/app.log
	fi
	echo "Starting the PHP Server on port 8000"
	php -S 0.0.0.0:8000 \
		-c ./.run/php/php.ini \
		-t public \
		public/index.php
}

#===============================================================================
function __web_start {
#===============================================================================
	if [ ! -d .provision/envs/dev ]; then
		mkdir .provision/envs/dev
	fi
	if [ ! -f .provision/envs/dev/config.ini ]; then
		cp .provision/envs/_template/config.ini .provision/envs/dev/config.ini
	fi
	if [ ! -f .provision/envs/dev/docker-boot.sh ]; then
		cp .provision/envs/_template/docker-boot.sh .provision/envs/dev/docker-boot.sh
	fi

	.provision/scripts/vs-configurator -l .provision/envs/dev/config.ini translate public/install.php.dist public/install.php
	.provision/scripts/vs-configurator -l .provision/envs/dev/config.ini translate public/index.php.dist public/index.php

	if ! docker ps -a | grep -q dragonfly-nginx; then
		echo "Creating the NGINX docker container"
		docker create \
			--name dragonfly-nginx \
			-p 8000:80 \
			--link dragonfly-mysql:mysql \
			-v $(pwd)/public:/data/www \
			-v $(pwd):/data/lib \
			-v $(pwd)/.provision/envs/dev:/data/etc \
			-v $(pwd)/.run/logs:/data/run/logs \
			vredens/ln_p
	fi
	if ! docker ps | grep -q dragonfly-nginx; then
		echo "Starting the NGINX docker container"
		docker start dragonfly-nginx
		# let the container instance boot
		sleep 3
		# show logs for the container boot
		cat .run/logs/boot.log
	fi
}

#===============================================================================
function __web_stop {
#===============================================================================
	if docker ps | grep -q dragonfly-nginx; then
		echo "Stoping the NGINX docker container"
		docker stop dragonfly-nginx
	fi
	if docker ps -a | grep -q dragonfly-nginx; then
		echo "Removing the NGINX docker container"
		docker rm -v dragonfly-nginx
		rm -f .run/logs/boot.log
	fi
}

set -e

case "$1" in
	'dev')
		case "$2" in
			'start')
				__install_deps
				__mysql_start
				__web_start
			;;

			'stop')
				__web_stop
				__mysql_stop
			;;

			'reset'|'clean')
				__mysql_destroy
				__remove_deps
				__web_stop
			;;

			'logs')
				tail -F .run/logs/{app,slim}.log
			;;

			*)
				echo "Usage: $0 dev start|stop|reset|logs"
			;;
		esac
	;;

	'mysql')
		case "$2" in
			'start')
				__mysql_start
			;;
			'stop')
				__mysql_stop
			;;
			'destroy')
				__mysql_destroy
			;;
		esac
	;;

	'web')
		case "$2" in
			'start')
				__web_start
			;;

			'stop')
				__web_stop
			;;
		esac
	;;

	'db'|'database')
		case "$2" in
			c*)
				__database_check
			;;
			i*)
				__database_install
			;;
			u*)
				__database_upgrade
			;;
		esac
	;;
esac
