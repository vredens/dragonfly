#!/bin/bash

create_rw_sub_folder logs
create_rw_sub_folder cache
create_rw_sub_folder php-sessions
create_rw_sub_folder config
create_rw_sub_folder files

# This file is specific to dev environment and used to setup configuration files.
/vs-configurator -l /data/etc/config.ini translate /data/lib/app/config/config.php.dist /data/run/config.php > /data/run/logs/boot.log 2>&1
